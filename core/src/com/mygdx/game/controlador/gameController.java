package com.mygdx.game.controlador;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.mygdx.game.modelo.Personaje;
import com.mygdx.game.modelo.ladrillo;
import com.mygdx.game.modelo.ladrillos;
import com.mygdx.game.modelo.pelota;
import com.mygdx.game.modelo.plataforma;

public class gameController implements ControllerListener, InputProcessor {
    public int nivel;
    boolean control;
    int[] changes;
    boolean td;
    int moveX;
    int moveY;

    public gameController(int nivel){
       this.nivel = nivel;
       Controllers.addListener(this);
       changes = new int[2];
       changes[0] = -1;
       changes[1] = -1;
       control = false;
       moveX = nivel;
       moveY = nivel;
       td = true;
    }

    public Object[] cambios(Object[] personajes, Graphics.DisplayMode dm){
        pelota ball = (pelota) personajes[1];
        plataforma platform = (plataforma) personajes[0];
        ladrillos pared = (ladrillos) personajes[2];
        switch (changes[0]){
            case 0:
                if(platform.personaje.getX()>=0 && platform.personaje.getX()+platform.personaje.getWidth()<=dm.width){
                    if(changes[1] == 13){
                        platform.personaje.translate(-10-nivel,0);
                        if(!ball.animado){
                            ball.personaje.translate(-10-nivel,0);
                        }
                    }
                    else{
                        if(changes[1] == 14){
                            platform.personaje.translate(10+nivel,0);
                            if(!ball.animado){
                                ball.personaje.translate(10+nivel, 0);
                            }
                        }
                        if(changes[1]== 0){
                            if(!ball.animado){
                                ball.animacion(new Texture("goopy.png"),17,1);
                                moveY = nivel;
                                ball.personaje.translate(moveX,moveY);
                            }
                            changes[0] = -1;
                            changes[1] = -1;
                        }
                    }
                }
                else{
                    if(platform.personaje.getX() <= 0){
                        platform.personaje.translateX(1);
                        if(!ball.animado){
                            ball.personaje.translateX(1);
                        }
                    }
                    if(platform.personaje.getX()+platform.personaje.getWidth()>=dm.width){
                        platform.personaje.translateX(-1);
                        if(!ball.animado){
                            ball.personaje.translateX(-1);
                        }
                    }
                }
                break;
            case 1:
                if(platform.personaje.getX()>=0 && platform.personaje.getX()+platform.personaje.getWidth()<=dm.width){
                    if(changes[1]<0){
                        platform.personaje.translate(-10-nivel,0);
                        if(!ball.animado){
                            ball.personaje.translate(-10-nivel,0);
                        }
                    }
                    else{
                        platform.personaje.translate(10+nivel,0);
                        if(!ball.animado){
                            ball.personaje.translate(10+nivel, 0);
                        }
                    }
                }
                else{
                    if(platform.personaje.getX() <= 0){
                        platform.personaje.translateX(1);
                        if(!ball.animado){
                            ball.personaje.translateX(1);
                        }
                    }
                    if(platform.personaje.getX()+platform.personaje.getWidth()>=dm.width){
                        platform.personaje.translateX(-1);
                        if(!ball.animado){
                            ball.personaje.translateX(-1);
                        }
                    }
                }
                break;
            case 2:
                if(!ball.animado ){
                    ball.animacion(new Texture("goopy.png"),17,1);
                    moveY = nivel;
                    ball.personaje.translate(moveX,moveY);
                }
                break;
            case 3:
                if(platform.personaje.getX()>=0 && platform.personaje.getX()+platform.personaje.getWidth()<=dm.width){
                    platform.personaje.setPosition(changes[1]-platform.personaje.getWidth()/2, platform.personaje.getY());
                    if(!ball.animado){
                        ball.personaje.setPosition(changes[1]-ball.personaje.getWidth()/2, ball.personaje.getY());
                    }
                    changes[0] = -1;
                    changes[1] = -1;
                }
                else{
                    if(platform.personaje.getX() <= 0){
                        platform.personaje.translateX(1);
                        if(!ball.animado){
                            ball.personaje.translateX(1);
                        }
                    }
                    if(platform.personaje.getX()+platform.personaje.getWidth()>=dm.width){
                        platform.personaje.translateX(-1);
                        if(!ball.animado){
                            ball.personaje.translateX(-1);
                        }
                    }
                }
                break;
        }
        if(ball.animado) {
            if (ball.personaje.getY() + ball.personaje.getHeight() <= 0) {
                ball.quieto(new Texture("goopy_quieto.png"));
                ball.personaje.setPosition(dm.width/2 - dm.width * 0.0125f/2, dm.height*0.1f + dm.height*0.025f);
                platform.personaje.setPosition(dm.width/2 - dm.width*0.15f/2, dm.height*0.1f);
                moveY = nivel;
            }
            else{
                if (ball.personaje.getX() + ball.personaje.getWidth() / 2 > platform.personaje.getX() && ball.personaje.getX() + ball.personaje.getWidth() / 2 < platform.personaje.getX() + platform.personaje.getWidth() && ball.personaje.getY() <= platform.personaje.getY() + dm.height*0.025 && ball.personaje.getY()>= platform.personaje.getY()) {
                    moveY *= -1;
                    Gdx.audio.newSound(Gdx.files.internal("boing.mp3")).play();
                }
                int i = 0;
                boolean sigue = true;
                ladrillo actual;
                while (i < pared.mapa.size() && sigue) {
                    actual = pared.mapa.get(i);
                    if (actual.activo&&ball.personaje.getX() + ball.personaje.getWidth()/2 >= actual.personaje.getX() && ball.personaje.getX()+ ball.personaje.getWidth()/2 <= actual.personaje.getX() + actual.personaje.getWidth()&&ball.personaje.getY()+ball.personaje.getHeight()>=actual.personaje.getY()&&ball.personaje.getY()<= actual.personaje.getY()+actual.personaje.getWidth()) {
                        if(actual.activo && actual.aguante>1){
                            actual.aguante--;
                            pared.valor(actual.aguante,actual,actual.personaje.getTexture());
                            Gdx.audio.newSound(Gdx.files.internal("golpe.mp3")).play();
                        }
                        else{
                            if(actual.aguante == 1){
                                actual.animacion(new Texture("explosion.png"),5,2);
                                Gdx.audio.newSound(Gdx.files.internal("boom.mp3")).play();
                                actual.activo = false;
                            }
                        }
                        if((ball.personaje.getY()+ball.personaje.getHeight()>=actual.personaje.getY()&&ball.personaje.getY()+ball.personaje.getHeight()<=actual.personaje.getY()+actual.personaje.getHeight()/4)||(ball.personaje.getY()<=actual.personaje.getY()+actual.personaje.getHeight()&&ball.personaje.getY()>=actual.personaje.getY()+actual.personaje.getHeight()-actual.personaje.getHeight()/4))
                            moveY *= -1;
                        else{
                            if(ball.personaje.getY()+ball.personaje.getHeight()>=actual.personaje.getY()&&ball.personaje.getY()<=actual.personaje.getY()+actual.personaje.getHeight())
                                moveX *= -1;
                        }
                        sigue = false;
                    }
                    i++;
                }
                if(ball.personaje.getX()<=1){
                    moveX = nivel;
                    Gdx.audio.newSound(Gdx.files.internal("golpe.mp3")).play();
                }
                if(ball.personaje.getX()+ball.personaje.getWidth()>= dm.width-1){
                    moveX = -nivel;
                    Gdx.audio.newSound(Gdx.files.internal("golpe.mp3")).play();
                }

                if(ball.personaje.getY()+ball.personaje.getHeight() >= dm.height-1){
                    moveY = -nivel;
                    Gdx.audio.newSound(Gdx.files.internal("golpe.mp3")).play();
                }
                ball.personaje.translate(moveX,moveY);
            }
        }
        personajes[0] = platform;
        personajes[1] = ball;
        personajes[2] = pared;
        return personajes;
    }

    @Override
    public void connected(Controller controller) {
    }

    @Override
    public void disconnected(Controller controller) {
    }

    @Override
    public boolean buttonDown(Controller controller, int buttonCode) {
        control = true;
        if(buttonCode == 0 || buttonCode == 13 || buttonCode == 14){
            changes[0] = 0;
            changes[1] = buttonCode;
        }
        return true;
    }

    @Override
    public boolean buttonUp(Controller controller, int buttonCode) {
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int axisCode, float value) {
        control = true;
        if(axisCode == 0 && (value < -0.90 || value > 0.90)){
            changes[0] = 1;
            changes[1] = (int) (5 * value);
        }
        return true;
    }

    @Override
    public boolean keyDown(int keycode) { return false; }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        control = false;
        changes[0] = 2;
        changes[1] = screenX;
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        control = false;
        control = false;
        changes[0] = 3;
        changes[1] = screenX;
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return false;
    }
}
