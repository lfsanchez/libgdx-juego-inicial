package com.mygdx.game.modelo;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class ladrillo extends Personaje{
    public boolean activo;
    public int aguante;
    public boolean animacion;

    public ladrillo(Texture textura, float X, float Y, float width, float height) {
        super(textura, X, Y, width, height);
        activo = true;
        animacion = true;
    }
}
